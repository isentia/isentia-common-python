PYTHON_RUNTIME = python3.8
VIRTUALENV_HOME = .venv
BIN_DIR = $(VIRTUALENV_HOME)/bin
SRC_DIR = .

# only for local testing, goes to sandbox
export TWINE_USERNAME ?= aws
export TWINE_PASSWORD ?= $(aws codeartifact get-authorization-token --domain isentia-poc --domain-owner 060207875941 --query authorizationToken --output text)
export TWINE_REPOSITORY_URL ?= https://isentia-poc-060207875941.d.codeartifact.ap-southeast-2.amazonaws.com/pypi/isentia-artifactory-poc/

APP_NAME ?= isentia-common-python

target:
	$(info ${HELP_MESSAGE})
	@exit 0

$(VIRTUALENV_HOME)/.deps:
	$(call create_venv) && $(call install_packages)

$(VIRTUALENV_HOME)/.dev_deps:
	$(call create_venv) && $(call install_test_packages)

venv:: $(VIRTUALENV_HOME)/.deps

clean::
	rm -rf $(VIRTUALENV_HOME) build dist .cache .eggs .tmp *.egg-info $(SRC_DIR)/*.egg-info .pytest_cache
	find . -name ".DS_Store" -exec rm -rf {} \; || true
	find . -name "*.pyc" -exec rm -rf {} \; || true
	find . -name "__pycache__" -exec rm -rf {} \; || true

run-dev: $(VIRTUALENV_HOME)/.deps $(VIRTUALENV_HOME)/.dev_deps
	$(BIN_DIR)/python -m tests.inspect_logging

test: $(VIRTUALENV_HOME)/.dev_deps
	# TODO

# Run Flake8 for static code analysis. Ignore errors that are part of black's job.
lint:: $(VIRTUALENV_HOME)/.dev_deps
	# TODO

build:: $(VIRTUALENV_HOME)/.dev_deps
	$(BIN_DIR)/python setup.py sdist

push:: $(VIRTUALENV_HOME)/.dev_deps
	$(BIN_DIR)/pip install twine --upgrade
	$(BIN_DIR)/twine upload dist/* --verbose

deploy:: $(VIRTUALENV_HOME)/.dev_deps
	@$(MAKE) build
	@$(MAKE) push

#############
#  Helpers  #
#############

define HELP_MESSAGE
	Common usage:

	...::: Execute unit tests and output test reports :::...
	$ make test

	...::: Build, and push library to Isentia PyPi server :::...
	$ make deploy
endef

# Creating Virtual env
define create_venv
	if [[ -d "./${VIRTUALENV_HOME}" ]]; then \
		echo "Virtual env already exists"; \
	else \
		${PYTHON_RUNTIME} -m venv ${VIRTUALENV_HOME}; \
	fi
endef

# Installing Packages
define install_packages
	$(BIN_DIR)/python -m pip install --upgrade pip
	$(BIN_DIR)/python -m pip install -r $(SRC_DIR)/requirements.txt
	touch $(VIRTUALENV_HOME)/.deps
endef

# Installing Test Packages
define install_test_packages
	$(BIN_DIR)/python -m pip install -r $(SRC_DIR)/requirements.dev.txt
	touch $(VIRTUALENV_HOME)/.dev_deps
endef

.PHONY:venv clean