import logging

from isentia_common.log import init_json_logging

if __name__ == "__main__":
    init_json_logging("my-app-name")

    logging.info("an info message", extra={"http_status_code": 200, "endpoint": "/api/no-problemo"})
    logging.warning("a warning message", extra={"http_status_code": 429})
    logging.error("an error message", extra={"http_status_code": 500})

    root_logger = logging.getLogger()
    root_logger.info(
        "an info message", extra={"http_status_code": 200, "endpoint": "/api/no-problemo"}
    )
    root_logger.warning("a warning message", extra={"http_status_code": 429})
    root_logger.error("an error message", extra={"http_status_code": 500})

    module_lvl_logger = logging.getLogger("special.module")
    module_lvl_logger.setLevel(logging.DEBUG)
    module_lvl_logger.debug("A debug message")
    module_lvl_logger.info(
        "an info message", extra={"http_status_code": 200, "endpoint": "/api/no-problemo"}
    )
    module_lvl_logger.warning("a warning message", extra={"http_status_code": 429})
    module_lvl_logger.error("an error message", extra={"http_status_code": 500})

    # logging using global namespace
    module_lvl_logger.info(
        "info with global", extra={"http_status_code": 200, "global": {"trace_id": 12345}}
    )

    raise EnvironmentError("something fatal happening in the environment!")
